#[derive(Debug, Default)]
pub struct Flags {
    pub shift: bool,
    pub ctrl: bool,
    pub alt: bool,
}
